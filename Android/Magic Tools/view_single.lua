-----------------------------------------------------------------------------------------
--
-- view1.lua
--
-----------------------------------------------------------------------------------------

require 'globalfunctions'

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	
	local board = createSingleBoard( 500 )
	board.x = display.contentWidth / 2
	board.y = display.contentHeight - board.height / 2 - 50
	group:insert( board )
	
	local yPosExtras = board.y - board.height / 2 - 120
	group:insert( createCoin(board.x - board.width / 2 + 30, yPosExtras, 120) )
	group:insert( createCoin(board.x - board.width / 2 + 180, yPosExtras, 120) )
	group:insert( createCoin(board.x - board.width / 2 + 330, yPosExtras, 120) )
	
	group:insert( createDie(board.x + board.width / 2 - 120 - 180, yPosExtras, 120, 6) )
	group:insert( createDie(board.x + board.width / 2 - 120 - 30, yPosExtras, 120, 20) )
end

playerBoardAspectRatio = 1006 / 504
boardHardcodeHeight = 550
function createSingleBoard ( height )
	local singleBoardGroup = display.newGroup()
	
	-- Board visuals
	local width = height * playerBoardAspectRatio
	display.newImageRect(singleBoardGroup, "images/PlayerField.png", width, height)
	
	-- Magic counters
	local counterWidth = 125 / boardHardcodeHeight * height
	local counterHeight = 241 / boardHardcodeHeight * height
	local offsetX = -width/2 + 37 / boardHardcodeHeight * height
	local offsetY = -height/2 + 34 / boardHardcodeHeight * height
	
	-- Colorless
	singleBoardGroup:insert(createCounter(offsetX, offsetY, counterWidth, counterHeight, 0, 64 / boardHardcodeHeight * height))
	-- Yellow
	singleBoardGroup:insert(createCounter(offsetX + counterWidth, offsetY, counterWidth, counterHeight, 0, 64 / boardHardcodeHeight * height))
	-- Blue
	singleBoardGroup:insert(createCounter(offsetX + counterWidth*2, offsetY, counterWidth, counterHeight, 0, 64 / boardHardcodeHeight * height))
	-- Black
	singleBoardGroup:insert(createCounter(offsetX, offsetY + counterHeight, counterWidth, counterHeight, 0, 64 / boardHardcodeHeight * height))
	-- Red
	singleBoardGroup:insert(createCounter(offsetX + counterWidth, offsetY + counterHeight, counterWidth, counterHeight, 0, 64 / boardHardcodeHeight * height))
	-- Green
	singleBoardGroup:insert(createCounter(offsetX + counterWidth*2, offsetY + counterHeight, counterWidth, counterHeight, 0, 64 / boardHardcodeHeight * height))
	
	-- Poison
	singleBoardGroup:insert(createCounter(
		-119 / boardHardcodeHeight * height,
		35 / boardHardcodeHeight * height,
		150 / boardHardcodeHeight * height,
		170 / boardHardcodeHeight * height,
		0, 64 / boardHardcodeHeight * height))
	
	-- Wins
	singleBoardGroup:insert(createCounter(
		320 / boardHardcodeHeight * height,
		-270 / boardHardcodeHeight * height,
		152 / boardHardcodeHeight * height,
		180 / boardHardcodeHeight * height,
		0, 64 / boardHardcodeHeight * height,
		false))
	
	-- Extra
	singleBoardGroup:insert(createCounter(
		288 / boardHardcodeHeight * height,
		-30 / boardHardcodeHeight * height,
		150 / boardHardcodeHeight * height,
		200 / boardHardcodeHeight * height,
		0, 64 / boardHardcodeHeight * height))
	
	-- Health
	singleBoardGroup:insert(createCounter(
		-118 / boardHardcodeHeight * height,
		-250 / boardHardcodeHeight * height,
		448 / boardHardcodeHeight * height,
		410 / boardHardcodeHeight * height,
		20, 128 / boardHardcodeHeight * height))
	
	return singleBoardGroup
end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- Do nothing
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. remove listeners, remove widgets, save state variables, etc.)
	
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene
