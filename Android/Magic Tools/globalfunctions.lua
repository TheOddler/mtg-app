startTouchX = 0
startTouchY = 0
currentSelectedCounter = nil

function newTextWithShadow(textOptions)
	
	-- create new group in the textOptions groups, then change that to the created group to the text will go in the textGroup
	local textGroup = display.newGroup()
	textOptions.parent:insert( textGroup )
	textOptions.parent = textGroup
	
	-- create text and shadow with offset
	textOptions.x = textOptions.x + textOptions.shadowOffset
	textOptions.y = textOptions.y + textOptions.shadowOffset
	local shadow = display.newText(textOptions)
	textOptions.x = textOptions.x - textOptions.shadowOffset
	textOptions.y = textOptions.y - textOptions.shadowOffset
	local text = display.newText(textOptions)
	shadow:setTextColor(0, 0, 0, 200)
	
	function textGroup:changeText(newText)
		text.text = newText
		shadow.text = newText
	end
	
	return textGroup
end

function createCounter( x, y, width, height, startValue, textSize, shouldReset )
	if shouldReset == nil then shouldReset = true end

	-- create objects
	local counterGroup = display.newGroup()
	local counterRect = display.newRect(counterGroup, x, y, width, height)
	local counterValue = startValue
	local textOptions = {
		parent = counterGroup,
		text = "", 
		x = counterRect.x,
		y = counterRect.y,
		width = width,	--required for multiline and alignment
		height = 0,		--required for multiline and alignment
		font = native.systemFontBold,   
		fontSize = textSize,
		shadowOffset = textSize/20,
		align = "center"          --new alignment field
	}
	local counterText = newTextWithShadow(textOptions)
	counterRect:setFillColor(0, 0, 0, 0)
	--counterRect:setFillColor(255, 0, 0, 55) --debug
	
	-- functionality
	function counterGroup:changeCounter(amount)
		counterValue = counterValue + amount
		if counterValue <= 0 then
			counterValue = 0
			counterText:changeText("")
		else
			counterText:changeText(counterValue)
		end
	end
	
	function swipe(event)
		if event.phase == "began" then
			startTouchX = event.x
            startTouchY = event.y
			currentSelectedCounter = counterGroup
        end
	end
	
	function reset(event)
		if shouldReset==true then
			counterValue = startValue
			counterGroup:changeCounter(0)
		end
	end
	
	counterRect:addEventListener( "touch", swipe )
	Runtime:addEventListener( "reset", reset )
	
	counterGroup:changeCounter(0)
	
	return counterGroup
end

coinFlipDuration = 600 --miliseconds
coinFlySizeMultiplyer = .5
coinFlipCount = 4
function createCoin( x, y, size )
	
	local counterGroup = display.newGroup()
	local counterRect = display.newRect(counterGroup, x, y, size, size)
	local image = display.newImageRect(counterGroup, "images/Coin.png", size, size)
		image.x = counterRect.x
		image.y = counterRect.y
	local counterValue
	if math.random() > .5 then counterValue = "H" else counterValue = "T" end
	local textOptions = {
		parent = counterGroup,
		text = counterValue, 
		x = counterRect.x,
		y = counterRect.y,
		width = size,	--required for multiline and alignment
		height = 0,		--required for multiline and alignment
		font = native.systemFontBold,   
		fontSize = size * .5,
		shadowOffset = size * .5 / 20,
		align = "center"          --new alignment field
	}
	local counterText = newTextWithShadow(textOptions)
	--counterRect:setFillColor(255, 0, 0, 55) --debug
	counterRect:setFillColor(0, 0, 0, 0)
	
	local flipping = false
	local flipStartTime
	function doFlip(event)
		if flipping == true then
			duration = event.time - flipStartTime
			if (duration < coinFlipDuration) then
				image.height = size * ( 1 + math.sin(duration/coinFlipDuration * math.pi)*coinFlySizeMultiplyer )
				image.width = (math.cos(duration/coinFlipDuration * math.pi * coinFlipCount)+1)/2 * image.height
			else
				image.height = size
				image.width = image.height
				
				if math.random() > .5 then counterText:changeText("H") else counterText:changeText("T") end
				flipping = false
			end
		end
	end
	
	function tap(event)
		if event.phase == "began" then
			if flipping == false then
				flipStartTime = event.time
				counterText:changeText("")
				counterGroup:toFront()
				flipping = true
			end
        end
	end
	
	Runtime:addEventListener( "enterFrame", doFlip )
	counterRect:addEventListener( "touch", tap )
	
	return counterGroup
end

dieRollDuration = 600 --miliseconds
function createDie( x, y, size, count )
	
	local counterGroup = display.newGroup()
	local counterRect = display.newRect(counterGroup, x, y, size, size)
	local image = display.newImageRect(counterGroup, "images/Die"..count..".png", size, size)
		image.x = counterRect.x
		image.y = counterRect.y
	local counterValue = math.random(1, count)
	local textOptions = {
		parent = counterGroup,
		text = counterValue, 
		x = counterRect.x,
		y = counterRect.y,
		width = size,	--required for multiline and alignment
		height = 0,		--required for multiline and alignment
		font = native.systemFontBold,   
		fontSize = size * .5,
		shadowOffset = size * .5 / 20,
		align = "center"          --new alignment field
	}
	local counterText = newTextWithShadow(textOptions)
	--counterRect:setFillColor(255, 0, 0, 55) --debug
	counterRect:setFillColor(0, 0, 0, 0)
	
	local rolling = false
	local rollStartTime
	function doRoll(event)
		if rolling == true then
			duration = event.time - rollStartTime
			if (duration < dieRollDuration) then
				counterText:changeText(math.random(1, count))
			else
				rolling = false
			end
		end
	end
	
	function tap(event)
		if event.phase == "began" then
			if rolling == false then
				rollStartTime = event.time
				rolling = true
			end
        end
	end
	
	Runtime:addEventListener( "enterFrame", doRoll )
	counterRect:addEventListener( "touch", tap )
	
	return counterGroup
end

function swipe(event)
	if event.phase == "ended" and currentSelectedCounter ~= nil then
		local swipeX = event.x - startTouchX
		local swipeY = event.y - startTouchY
		local swipeDir = math.atan2(swipeY, swipeX) / math.pi * 180
		local boardDir = currentSelectedCounter.parent.rotation
		local relDirection = swipeDir - boardDir
		
		while relDirection < 0 do
			relDirection = relDirection + 360
		end
		
		--print ( "-----" )
		--print (swipeDir)
		--print (boardDir)
		--print (relDirection)
		
		if relDirection > 0 and relDirection < 180 then
			currentSelectedCounter:changeCounter(-1)
		else
			currentSelectedCounter:changeCounter(1)
		end
		
		currentSelectedCounter = nil
	end
end
Runtime:addEventListener( "touch", swipe )


function onShake(event)
	if event.isShake then
		local event = { name = "reset" }
		Runtime:dispatchEvent( event )
	end
end
Runtime:addEventListener("accelerometer", onShake)



local json = require("json")
function saveTable(t, filename)
    local path = system.pathForFile( filename, system.DocumentsDirectory)
    local file = io.open(path, "w")
    if file then
        local contents = json.encode(t)
        file:write( contents )
        io.close( file )
        return true
    else
        return false
    end
end
	
function loadTable(filename)
    local path = system.pathForFile( filename, system.DocumentsDirectory)
    local contents = ""
    local myTable = {}
    local file = io.open( path, "r" )
    if file then
         -- read all contents of file into a string
         local contents = file:read( "*a" )
         myTable = json.decode(contents);
         io.close( file )
         return myTable 
    end
    return nil
end