application = {
	content = {
		width = 768,
		height = 768 * display.pixelHeight / display.pixelWidth,
		scale = "letterbox", --"letterbox",--"zoomStretch",
		fps = 15,
		
        imageSuffix = {
		    ["@s"] = .5,
		}
	},
	
	--license = {
	--	google = {
	--		key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAju6HzLKMFYRPe1Jbqu/wrww1F+EosIQ7FXeWI4WR3DFMtwqzXVw+UdmZTzP3M3jyR/4WusPxMSan4cM+D4Ibwtu1DpsUxw7eh8V3IIG5cDPG1Tn4OC+RQl7QyPqp11pNoAEUIFFF/QcWKf+mn26NxWWHhlZhq0/3h8bnKOePhGqyRhUOVRJv5Na+QrgKG7dQxlr5GZeW6Wx99VrK4/3JIb06FCHsyokCTZumhN6IPD/83+rI2rb4m+vGkGrYlypsf+LjTS+Ggs1bve0VaR9fZqMzMqRO9vHKohnRXsaFFjOFonxB65AHMZiVQDp2AZwNIaGgtuZIcMNUhT6BfbQN7wIDAQAB",
	--		policy = "serverManaged",
	--	},
	--},

    --[[
    -- Push notifications

    notification =
    {
        iphone =
        {
            types =
            {
                "badge", "sound", "alert", "newsstand"
            }
        }
    }
    --]]    
}
