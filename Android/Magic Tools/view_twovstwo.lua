-----------------------------------------------------------------------------------------
--
-- view1.lua
--
-----------------------------------------------------------------------------------------

require 'view_single'

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	
	local boardOne = createSingleBoard( 250 )
	boardOne.x = display.contentWidth - boardOne.width / 2
	boardOne.y = display.contentHeight - boardOne.height / 2 - 50 --50 for the tab bar
	
	local boardTwo = createSingleBoard( 250 )
	boardTwo.x = boardTwo.width / 2
	boardTwo.y = display.contentHeight - boardTwo.height / 2 - 50
	
	local boardThree = createSingleBoard( 250 )
	boardThree:rotate(180)
	boardThree.x = display.contentWidth - boardThree.width / 2
	boardThree.y = boardThree.height / 2 + 50
	
	local boardFour = createSingleBoard( 250 )
	boardFour:rotate(180)
	boardFour.x = boardFour.width / 2
	boardFour.y = boardFour.height / 2 + 50
	
	group:insert( boardOne )
	group:insert( boardTwo )
	group:insert( boardThree )
	group:insert( boardFour )
	
	local extrasSize = 120
	local xPosExtras = (display.contentWidth - extrasSize) / 2
	local yPosExtras = (display.contentHeight - extrasSize) / 2
	group:insert( createCoin(xPosExtras - 30, yPosExtras, extrasSize) )
	group:insert( createCoin(xPosExtras - 180, yPosExtras, extrasSize) )
	group:insert( createCoin(xPosExtras - 330, yPosExtras, extrasSize) )
	
	group:insert( createDie(xPosExtras + 120 + 180, yPosExtras, extrasSize, 6) )
	group:insert( createDie(xPosExtras + 120 + 30, yPosExtras, extrasSize, 20) )
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- Do nothing
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. remove listeners, remove widgets, save state variables, etc.)
	
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene
