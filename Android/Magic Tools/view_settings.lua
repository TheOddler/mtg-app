-----------------------------------------------------------------------------------------
--
-- view1.lua
--
-----------------------------------------------------------------------------------------

require 'globalfunctions'
require 'main'

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	
	local title = display.newText(group, "Settings:", 40, 80, "Helvetica", 50 )
	title:setTextColor( 0, 0, 0, 200 )
	
	simpleModeFunction = function(value) gameSettings.simpleMode = value end
	simpleModeCheckbox = createSettingsCheckbox("Simplified tab-laybout", gameSettings.simpleMode, simpleModeFunction, 50, 180, 80)
	
	showHelpFunction = function(value) gameSettings.showHelp = value end
	showHelpCheckbox = createSettingsCheckbox("Show help tab", gameSettings.showHelp, showHelpFunction, 50, 330, 80)
	
	local notice = display.newText(group, "Changes require restart, sorry.", 40, display.contentHeight - 150, "Helvetica", 50 )
	notice:setTextColor( 0, 0, 0, 200 )
	
	group:insert(showHelpCheckbox)
	group:insert(simpleModeCheckbox)
	
end

options = {
	width = 128,
	height = 128,
	numFrames = 2,
	sheetContentWidth = 256,  -- width of original 1x size of entire sheet
	sheetContentHeight = 128  -- height of original 1x size of entire sheet
}
checkboxImages = graphics.newImageSheet( "images/settings/Checkbox.png", options )
function createSettingsCheckbox (name, startingValue, changeFunction, x, y, size)
	local counterGroup = display.newGroup()
	local checkboxRect = display.newRect(counterGroup, x, y, display.contentWidth - x*2, size)
	
	local imageChecked = display.newImage(counterGroup, checkboxImages, 1, size, size)
	local imageUnchecked = display.newImage(counterGroup, checkboxImages, 2, size, size)
	imageChecked.x = x + size/2
	imageChecked.y = y + size/2
	imageChecked.width = size
	imageChecked.height = size
	imageUnchecked.x = x + size/2
	imageUnchecked.y = y + size/2
	imageUnchecked.width = size
	imageUnchecked.height = size
	if startingValue then
		imageChecked.isVisible = true
		imageUnchecked.isVisible = false
	else
		imageChecked.isVisible = false
		imageUnchecked.isVisible = true
	end
	
	local text = display.newText(counterGroup, name, x + size + 10, y, "Helvetica", size * .9 )
	text:setTextColor( 0, 0, 0, 200 )
	
	--checkboxRect:setFillColor(255, 0, 0, 55) --debug
	checkboxRect:setFillColor(0, 0, 0, 0)
	
	function tap(event)
		if event.phase == "began" then
			if startingValue then
				startingValue = false
				imageChecked.isVisible = false
				imageUnchecked.isVisible = true
			else
				startingValue = true
				imageChecked.isVisible = true
				imageUnchecked.isVisible = false
			end
			changeFunction(startingValue)
			saveTable(gameSettings, "MagicToolsSettings.json")
		end
	end

	checkboxRect:addEventListener( "touch", tap)
	return counterGroup
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- Do nothing
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. remove listeners, remove widgets, save state variables, etc.)
	
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene
