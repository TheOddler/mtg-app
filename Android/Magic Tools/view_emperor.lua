-----------------------------------------------------------------------------------------
--
-- view1.lua
--
-----------------------------------------------------------------------------------------

require 'view_single'

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

function scene:createScene( event )
	local group = self.view
	
	local extrasSize = 120
	local xPosExtras = display.contentWidth - extrasSize - 30
	local yPosExtras = (display.contentHeight - extrasSize) / 2
	
	local boardOne = createEmperorBoard( 327 )
	boardOne:rotate(180)
	boardOne.x = (display.contentWidth - extrasSize - 30) / 2
	boardOne.y = boardOne.height / 2 + 50
	
	local boardTwo = createEmperorBoard( 327 )
	boardTwo.x = boardOne.x
	boardTwo.y = display.contentHeight - boardTwo.height / 2 - 50
	
	group:insert( boardOne )
	group:insert( boardTwo )
	
	
	group:insert( createCoin(xPosExtras, yPosExtras - boardOne.height + extrasSize / 2 + 10, extrasSize) )
	group:insert( createCoin(xPosExtras, yPosExtras - boardOne.height + extrasSize / 2 + 130, extrasSize) )
	group:insert( createCoin(xPosExtras, yPosExtras - boardOne.height + extrasSize / 2 + 250, extrasSize) )
	
	group:insert( createDie(xPosExtras, yPosExtras + boardOne.height - extrasSize / 2 - 10, extrasSize, 6) )
	group:insert( createDie(xPosExtras, yPosExtras + boardOne.height - extrasSize / 2 - 130, extrasSize, 20) )
end

emperorBoardAspectRatio = 1612 / 654
emperorBoardHardcodeHeight = 654
function createEmperorBoard ( height )
	local boardGroup = display.newGroup()
	
	-- Board visuals
	local width = height * emperorBoardAspectRatio
	display.newImageRect(boardGroup, "images/EmperorField.png", width, height)
	
	-- Magic counters
	local counterWidth = 115 / emperorBoardHardcodeHeight * height
	local counterHeight = 230 / emperorBoardHardcodeHeight * height
	local offsetX = -width/2 + 39 / emperorBoardHardcodeHeight * height
	local offsetY = 64 / emperorBoardHardcodeHeight * height
	local offsetXTwo = 418 / emperorBoardHardcodeHeight * height
	-- Colorless
	boardGroup:insert(createCounter(offsetX, offsetY, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Yellow
	boardGroup:insert(createCounter(offsetX + counterWidth, offsetY, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Blue
	boardGroup:insert(createCounter(offsetX + counterWidth*2, offsetY, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Black
	boardGroup:insert(createCounter(offsetXTwo, offsetY, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Red
	boardGroup:insert(createCounter(offsetXTwo + counterWidth, offsetY, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Green
	boardGroup:insert(createCounter(offsetXTwo + counterWidth*2, offsetY, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	
	-- Poison
	boardGroup:insert(createCounter(
		-320 / emperorBoardHardcodeHeight * height,
		98 / emperorBoardHardcodeHeight * height,
		140 / emperorBoardHardcodeHeight * height,
		180 / emperorBoardHardcodeHeight * height,
		0, 64 / emperorBoardHardcodeHeight * height))
	
	-- Wins
	boardGroup:insert(createCounter(
		175 / emperorBoardHardcodeHeight * height,
		85 / emperorBoardHardcodeHeight * height,
		152 / emperorBoardHardcodeHeight * height,
		200 / emperorBoardHardcodeHeight * height,
		0, 64 / emperorBoardHardcodeHeight * height,
		false))
	
	-- Extra
	boardGroup:insert(createCounter(
		232 / emperorBoardHardcodeHeight * height,
		-130 / emperorBoardHardcodeHeight * height,
		150 / emperorBoardHardcodeHeight * height,
		190 / emperorBoardHardcodeHeight * height,
		0, 64 / emperorBoardHardcodeHeight * height))
		
	-- Extra
	boardGroup:insert(createCounter(
		-384 / emperorBoardHardcodeHeight * height,
		-130 / emperorBoardHardcodeHeight * height,
		150 / emperorBoardHardcodeHeight * height,
		190 / emperorBoardHardcodeHeight * height,
		0, 64 / emperorBoardHardcodeHeight * height))
	
	
	-- Health Emperor
	boardGroup:insert(createCounter(
		-224 / emperorBoardHardcodeHeight * height,
		-160 / emperorBoardHardcodeHeight * height,
		448 / emperorBoardHardcodeHeight * height,
		410 / emperorBoardHardcodeHeight * height,
		20, 128 / emperorBoardHardcodeHeight * height))
	
	-- Health Soldier Left
	boardGroup:insert(createCounter(
		-760 / emperorBoardHardcodeHeight * height,
		-300 / emperorBoardHardcodeHeight * height,
		448 / emperorBoardHardcodeHeight * height,
		330 / emperorBoardHardcodeHeight * height,
		20, 128 / emperorBoardHardcodeHeight * height))
	
	-- Health Soldier Right
	boardGroup:insert(createCounter(
		320 / emperorBoardHardcodeHeight * height,
		-300 / emperorBoardHardcodeHeight * height,
		448 / emperorBoardHardcodeHeight * height,
		330 / emperorBoardHardcodeHeight * height,
		20, 128 / emperorBoardHardcodeHeight * height))
	
	return boardGroup
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- Do nothing
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. remove listeners, remove widgets, save state variables, etc.)
	
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene
