--local licensing = require( "licensing" )
--licensing.init( "google" )
--
--local function licensingListener( event )
--   local verified = event.isVerified
--   if not event.isVerified then
--      --failed verify app from the play store, we print a message
--      print( "Oh you naughty boy." )
--      native.requestExit()  --assuming this is how we handle pirates
--   end
--end
--licensing.verify( licensingListener )

-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------
require 'globalfunctions'
require 'tilebg'


-- show default status bar (iOS)
display.setStatusBar( display.DarkStatusBar ) --display.DefaultStatusBar

-- include Corona's "widget" library
local widget = require "widget"
local storyboard = require "storyboard"


-- Load settings
gameSettings = loadTable("MagicToolsSettings.json")
if (gameSettings == nil) then
	--create new default settings file
	gameSettings = {}
	gameSettings.lastOpened = "Help"
	gameSettings.simpleMode = false
	gameSettings.showHelp = true
	
	saveTable(gameSettings, "MagicToolsSettings.json")
end


-- event listeners for tab buttons:
local function onSingePlayerTabPress( event )
	storyboard.gotoScene( "view_single" )
	gameSettings.lastOpened = "Single"
	saveTable(gameSettings, "MagicToolsSettings.json")
end

local function onTwoPlayerTabPress( event )
	storyboard.gotoScene( "view_twoplayers" )
	gameSettings.lastOpened = "TwoPlayer"
	saveTable(gameSettings, "MagicToolsSettings.json")
end

local function onTwoVSTwoTabPress( event )
	storyboard.gotoScene( "view_twovstwo" )
	gameSettings.lastOpened = "TwoVSTwo"
	saveTable(gameSettings, "MagicToolsSettings.json")
end

local function onFourPlayersTabPress( event )
	storyboard.gotoScene( "view_fourplayers" )
	gameSettings.lastOpened = "FourPlayer"
	saveTable(gameSettings, "MagicToolsSettings.json")
end

local function onEmperorTabPress( event )
	storyboard.gotoScene( "view_emperor" )
	gameSettings.lastOpened = "Emperor"
	saveTable(gameSettings, "MagicToolsSettings.json")
end

local function onTwoHeadedGiantTabPress( event )
	storyboard.gotoScene( "view_twoheadedgiant" )
	gameSettings.lastOpened = "TwoHeadedGiant"
	saveTable(gameSettings, "MagicToolsSettings.json")
end

local function onHelpTabPress( event )
	storyboard.gotoScene( "view_help" )
	gameSettings.lastOpened = "Help"
	saveTable(gameSettings, "MagicToolsSettings.json")
end

local function onSettingsTabPress( event )
	storyboard.gotoScene( "view_settings" )
	gameSettings.lastOpened = "Settings"
	saveTable(gameSettings, "MagicToolsSettings.json")
end



-- Create background
local background = tileBG("images/Background.png", 480, 457)

-- create a tabBar widget with two buttons at the bottom of the screen

-- table to setup buttons
local tabButtons = {}

if gameSettings.showHelp == true then
	table.insert(tabButtons,	{ --label="Help",
		defaultFile = "images/tabs/Help.png",
		overFile = "images/tabs/Help.png",
		width = 46, height = 32,
		onPress=onHelpTabPress, selected=(gameSettings.lastOpened=="Help") })
end
	
table.insert(tabButtons,	{ --label="Single",
	defaultFile = "images/tabs/SinglePlayer.png",
	overFile = "images/tabs/SinglePlayer.png",
	width = 46, height = 32,
	onPress=onSingePlayerTabPress, selected=(gameSettings.lastOpened=="Single") })

if gameSettings.simpleMode == false then
	table.insert(tabButtons,	{ --label="Two Player",
		defaultFile = "images/tabs/TwoPlayers.png",
		overFile = "images/tabs/TwoPlayers.png",
		width = 46, height = 32,
		onPress=onTwoPlayerTabPress, selected=(gameSettings.lastOpened=="TwoPlayer") })
	table.insert(tabButtons,	{ --label="Two vs Two",
		defaultFile = "images/tabs/TwoVsTwo.png",
		overFile = "images/tabs/TwoVsTwo.png",
		width = 46, height = 32,
		onPress=onTwoVSTwoTabPress, selected=(gameSettings.lastOpened=="TwoVSTwo") })
	table.insert(tabButtons,	{ --label="Four Players",
		defaultFile = "images/tabs/FourPlayers.png",
		overFile = "images/tabs/FourPlayers.png",
		width = 46, height = 32,
		onPress=onFourPlayersTabPress, selected=(gameSettings.lastOpened=="FourPlayer") })
	table.insert(tabButtons,	{ --label="Emperor",
		defaultFile = "images/tabs/Emperor.png",
		overFile = "images/tabs/Emperor.png",
		width = 46, height = 32,
		onPress=onEmperorTabPress, selected=(gameSettings.lastOpened=="Emperor") })
	table.insert(tabButtons,	{ --label="Two Headed Giant",
		defaultFile = "images/tabs/TwoHeadedGiant.png",
		overFile = "images/tabs/TwoHeadedGiant.png",
		width = 46, height = 32,
		onPress=onTwoHeadedGiantTabPress, selected=(gameSettings.lastOpened=="TwoHeadedGiant") })
end
		
table.insert(tabButtons,	{ --label="Settings",
	defaultFile = "images/tabs/Settings.png",
	overFile = "images/tabs/Settings.png",
	width = 46, height = 32,
	onPress=onSettingsTabPress, selected=(gameSettings.lastOpened=="Settings") })



-- create the actual tabBar widget
local tabBar = widget.newTabBar{
	height = 50,
	top = display.contentHeight - 50,	-- 50 is default height for tabBar widget
	buttons = tabButtons
}

if gameSettings.lastOpened=="Single" then onSingePlayerTabPress()
elseif gameSettings.lastOpened=="TwoPlayer" then onTwoPlayerTabPress()
elseif gameSettings.lastOpened=="TwoVSTwo" then onTwoVSTwoTabPress()
elseif gameSettings.lastOpened=="FourPlayer" then onFourPlayersTabPress()
elseif gameSettings.lastOpened=="Emperor" then onEmperorTabPress()
elseif gameSettings.lastOpened=="TwoHeadedGiant" then onTwoHeadedGiantTabPress()
elseif gameSettings.lastOpened=="Help" then onHelpTabPress()
elseif gameSettings.lastOpened=="Settings" then onSettingsTabPress()
end