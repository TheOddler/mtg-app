-----------------------------------------------------------------------------------------
--
-- view2.lua
--
-----------------------------------------------------------------------------------------

require 'view_single'

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
emperorBoardAspectRatio = 1612 / 504
emperorBoardHardcodeHeight = 504

function scene:createScene( event )
	local group = self.view
	
	local extrasSize = 120
	local xPosExtras = display.contentWidth - extrasSize - 30
	local yPosExtras = (display.contentHeight - extrasSize) / 2
	
	local boardOne = createTHGBoard( math.min(327, xPosExtras/emperorBoardAspectRatio) )
	boardOne:rotate(180)
	boardOne.x = (display.contentWidth - extrasSize - 30) / 2
	boardOne.y = boardOne.height / 2 + 50
	
	local boardTwo = createTHGBoard( boardOne.height )
	boardTwo.x = boardOne.x
	boardTwo.y = display.contentHeight - boardTwo.height / 2 - 50
	
	group:insert( boardOne )
	group:insert( boardTwo )
	
	
	group:insert( createCoin(xPosExtras, yPosExtras - display.contentHeight/2 + extrasSize, extrasSize) )
	group:insert( createCoin(xPosExtras, yPosExtras - display.contentHeight/2 + extrasSize + 120, extrasSize) )
	group:insert( createCoin(xPosExtras, yPosExtras - display.contentHeight/2 + extrasSize + 240, extrasSize) )
	
	group:insert( createDie(xPosExtras, yPosExtras + display.contentHeight/2 - extrasSize, extrasSize, 6) )
	group:insert( createDie(xPosExtras, yPosExtras + display.contentHeight/2 - extrasSize - 120, extrasSize, 20) )
end

function createTHGBoard ( height )
	local boardGroup = display.newGroup()
	
	-- Board visuals
	local width = height * emperorBoardAspectRatio
	display.newImageRect(boardGroup, "images/TwoHeadedGiantField.png", width, height)
	
	-- Magic counters
	local counterWidth = 115 / emperorBoardHardcodeHeight * height
	local counterHeight = 220 / emperorBoardHardcodeHeight * height
	local offsetX = -771 / emperorBoardHardcodeHeight * height
	local offsetY = -220 / emperorBoardHardcodeHeight * height
	local offsetXTwo = 423 / emperorBoardHardcodeHeight * height
	-- Colorless
	boardGroup:insert(createCounter(offsetX, offsetY, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Yellow
	boardGroup:insert(createCounter(offsetX + counterWidth, offsetY, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Blue
	boardGroup:insert(createCounter(offsetX + counterWidth*2, offsetY, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Black
	boardGroup:insert(createCounter(offsetX, offsetY+counterHeight, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Red
	boardGroup:insert(createCounter(offsetX + counterWidth, offsetY+counterHeight, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Green
	boardGroup:insert(createCounter(offsetX + counterWidth*2, offsetY+counterHeight, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	
	-- Colorless
	boardGroup:insert(createCounter(offsetXTwo, offsetY, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Yellow
	boardGroup:insert(createCounter(offsetXTwo + counterWidth, offsetY, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Blue
	boardGroup:insert(createCounter(offsetXTwo + counterWidth*2, offsetY, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Black
	boardGroup:insert(createCounter(offsetXTwo, offsetY+counterHeight, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Red
	boardGroup:insert(createCounter(offsetXTwo + counterWidth, offsetY+counterHeight, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	-- Green
	boardGroup:insert(createCounter(offsetXTwo + counterWidth*2, offsetY+counterHeight, counterWidth, counterHeight, 0, 64 / emperorBoardHardcodeHeight * height))
	
	-- Poison
	boardGroup:insert(createCounter(
		-330 / emperorBoardHardcodeHeight * height,
		32 / emperorBoardHardcodeHeight * height,
		160 / emperorBoardHardcodeHeight * height,
		160 / emperorBoardHardcodeHeight * height,
		0, 64 / emperorBoardHardcodeHeight * height))
	
	-- Wins
	boardGroup:insert(createCounter(
		176 / emperorBoardHardcodeHeight * height,
		10 / emperorBoardHardcodeHeight * height,
		150 / emperorBoardHardcodeHeight * height,
		200 / emperorBoardHardcodeHeight * height,
		0, 64 / emperorBoardHardcodeHeight * height,
		false))
	
	-- Extra
	boardGroup:insert(createCounter(
		232 / emperorBoardHardcodeHeight * height,
		-206 / emperorBoardHardcodeHeight * height,
		150 / emperorBoardHardcodeHeight * height,
		190 / emperorBoardHardcodeHeight * height,
		0, 64 / emperorBoardHardcodeHeight * height))
		
	-- Extra 2
	boardGroup:insert(createCounter(
		-384 / emperorBoardHardcodeHeight * height,
		-206 / emperorBoardHardcodeHeight * height,
		150 / emperorBoardHardcodeHeight * height,
		190 / emperorBoardHardcodeHeight * height,
		0, 64 / emperorBoardHardcodeHeight * height))
	
	
	-- Health Emperor
	boardGroup:insert(createCounter(
		-218 / emperorBoardHardcodeHeight * height,
		-230 / emperorBoardHardcodeHeight * height,
		440 / emperorBoardHardcodeHeight * height,
		360 / emperorBoardHardcodeHeight * height,
		20, 128 / emperorBoardHardcodeHeight * height))
	
	return boardGroup
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- do nothing
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. remove listeners, remove widgets, save state variables, etc.)
	
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene
