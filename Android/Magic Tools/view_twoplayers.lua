-----------------------------------------------------------------------------------------
--
-- view1.lua
--
-----------------------------------------------------------------------------------------

require 'view_single'

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()

-----------------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
-- 
-- NOTE: Code outside of listener functions (below) will only be executed once,
--		 unless storyboard.removeScene() is called.
-- 
-----------------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
	
	-- TODO : Rotation breaks swiping direction
	local boardOne = createSingleBoard( 300 )
	boardOne:rotate(-90)
	boardOne.x = display.contentWidth - boardOne.height / 2
	boardOne.y = display.contentHeight / 2
	
	local boardTwo = createSingleBoard( 300 )
	boardTwo:rotate(90)
	boardTwo.x = boardTwo.height / 2
	boardTwo.y = boardOne.y
	
	group:insert( boardOne )
	group:insert( boardTwo )
	
	local extrasSize = 120
	local xPosExtras = (display.contentWidth - extrasSize) / 2
	local yPosExtras = (display.contentHeight - extrasSize) / 2
	group:insert( createCoin(xPosExtras - 70, yPosExtras - boardOne.width / 2 + 90, extrasSize) )
	group:insert( createCoin(xPosExtras + 70, yPosExtras - boardOne.width / 2 + 120, extrasSize) )
	group:insert( createCoin(xPosExtras - 30, yPosExtras - boardOne.width / 2 + 220, extrasSize) )
	
	group:insert( createDie(xPosExtras - 65, yPosExtras + boardOne.width / 2 - 100, extrasSize, 6) )
	group:insert( createDie(xPosExtras + 65, yPosExtras + boardOne.width / 2 - 150, extrasSize, 20) )
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- Do nothing
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. remove listeners, remove widgets, save state variables, etc.)
	
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene
