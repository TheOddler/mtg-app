function SinglePlayerBattlegroundiPhone() {
	var battleGround = Ti.UI.createWindow({  
	    title:'4FFA',
	    backgroundColor:'#37474A',
	    orientationModes: [ Ti.UI.LANDSCAPE_RIGHT ],
	    fullscreen: true,
	    navBarHidden: true,
	});
	
	
	// Reset Button
	resetButton = new ResetButton(0, 0, 90);
	battleGround.add(resetButton.getView());
	
	
	// Playerfield
	var scale = 0.95427435387674; //width / PLAYER_FIELD_WIDTH;
	var playerFieldOne = new PlayerField(xMidForPlayerField, height - PLAYER_FIELD_HEIGHT, 0, scale);
	battleGround.add(playerFieldOne.getView());
	
	// Coins
	var otherScale = 0.8;
	var coinY = COIN_SIZE * 0.1;
	
	var coinOne = new Coin(width / 8.0 * 1.2, coinY, otherScale, 0);
	battleGround.add(coinOne.getView());
	
	var coinTwo = new Coin(width / 8.0 * 2.2, coinY, otherScale, 0);
	battleGround.add(coinTwo.getView());
	
	var coinThree = new Coin(width / 8.0 * 3.2, coinY, otherScale, 0);
	battleGround.add(coinThree.getView());
	
	// Dies
	var dieY = -DIE_SIZE * 0.05;
	var dieFour = new Die(width / 8.0 * 4.8, dieY, 6, 'images/Die6.png', otherScale, 0);
	battleGround.add(dieFour.getView());
	
	var dieTwenty = new Die(width / 8.0 * 6.0, dieY, 20, 'images/Die20.png', otherScale, 0);
	battleGround.add(dieTwenty.getView());
	
	return battleGround;
}

function SinglePlayerBattleground() {
	var battleGround = Ti.UI.createWindow({  
	    title:'4FFA',
	    backgroundColor:'#37474A',
	    orientationModes: [ Ti.UI.LANDSCAPE_RIGHT ],
	    fullscreen: true,
	    navBarHidden: true,
	});
	
	
	// Reset Button
	resetButton = new ResetButton(0, 0, 90);
	battleGround.add(resetButton.getView());
	
	
	// Playerfield
	var scale = width / PLAYER_FIELD_WIDTH;
	var playerFieldOne = new PlayerField(xMidForPlayerField, height - PLAYER_FIELD_HEIGHT, 0, scale);
	battleGround.add(playerFieldOne.getView());
	
	// Coins
	var otherScale = 0.8;
	var coinY = COIN_SIZE * 0.1;
	
	var coinOne = new Coin(width / 8.0 * 1.2, coinY, otherScale, 0);
	battleGround.add(coinOne.getView());
	
	var coinTwo = new Coin(width / 8.0 * 2.2, coinY, otherScale, 0);
	battleGround.add(coinTwo.getView());
	
	var coinThree = new Coin(width / 8.0 * 3.2, coinY, otherScale, 0);
	battleGround.add(coinThree.getView());
	
	// Dies
	var dieY = -DIE_SIZE * 0.05;
	var dieFour = new Die(width / 8.0 * 4.8, dieY, 6, 'images/Die6.png', otherScale, 0);
	battleGround.add(dieFour.getView());
	
	var dieTwenty = new Die(width / 8.0 * 6.0, dieY, 20, 'images/Die20.png', otherScale, 0);
	battleGround.add(dieTwenty.getView());
	
	return battleGround;
}

function TwoPlayerBattleground() {
	// Window
	var battleGround = Ti.UI.createWindow({  
	    title:'4FFA',
	    backgroundColor:'#37474A',
	    orientationModes: [ Ti.UI.LANDSCAPE_RIGHT ],
	    fullscreen: true,
	    navBarHidden: true,
	});
	
	
	// Reset Button
	resetButton = new ResetButton((width - RESET_BUTTON_SIZE) / 2.0, 0, 90);
	battleGround.add(resetButton.getView());
	
	
	// Settings
	var scale = height / PLAYER_FIELD_WIDTH;
	
	// Playerfields
	var playerFieldOne = new PlayerField(width - PLAYER_FIELD_HEIGHT * scale - 75, yMidForPlayerField, -90, scale);
	battleGround.add(playerFieldOne.getView());
	
	var playerFieldTwo = new PlayerField(-70, yMidForPlayerField, 90, scale);
	battleGround.add(playerFieldTwo.getView());
	
	// Coins
	var coinX = width / 2.0 - COIN_SIZE * 0.6;
	var coinY = height * 3.5 / 5.0;
	
	var coinOne = new Coin(coinX + COIN_SIZE * 0.4, coinY - COIN_SIZE * 0.28);
	battleGround.add(coinOne.getView());
	
	var coinTwo = new Coin(coinX + COIN_SIZE * 0.9, coinY + COIN_SIZE * 0.75);
	battleGround.add(coinTwo.getView());
	
	var coinThree = new Coin(coinX - COIN_SIZE * 0.4, coinY + COIN_SIZE * 0.6);
	battleGround.add(coinThree.getView());
	
	// Dies
	var dieX = width / 2.0 - DIE_SIZE * 0.3;
	var dieY = height * 2.0 / 9.0;
	
	var dieFour = new Die(dieX + DIE_SIZE * 0.2, dieY + DIE_SIZE * 0.6, 6, 'images/Die6.png');
	battleGround.add(dieFour.getView());
	
	var dieTwenty = new Die(dieX - DIE_SIZE * 0.2, dieY - DIE_SIZE * 0.5, 20, 'images/Die20.png');
	battleGround.add(dieTwenty.getView());
	
	// Return the battleground
	return battleGround;
}

function FourPlayerBattleground() {
	// Window
	var battleGround = Ti.UI.createWindow({  
	    title:'4FFA',
	    backgroundColor:'#37474A',
	    orientationModes: [ Ti.UI.LANDSCAPE_RIGHT ],
	    fullscreen: true,
	    navBarHidden: true,
	});
	
	
	// Reset Button
	resetButton = new ResetButton(0, 0, 90);
	battleGround.add(resetButton.getView());
	
	
	// Settings
	var scale = 1.05;
	
	// Playerfields
	var playerFieldOne = new PlayerField(xMidForPlayerField, 0, 180, scale);
	battleGround.add(playerFieldOne.getView());
	
	var playerFieldTwo = new PlayerField(xMidForPlayerField, height - PLAYER_FIELD_HEIGHT, 0, scale);
	battleGround.add(playerFieldTwo.getView());
	
	var playerFieldThree = new PlayerField(width - PLAYER_FIELD_WIDTH + PLAYER_FIELD_HEIGHT / 2.0, yMidForPlayerField, -90, scale);
	battleGround.add(playerFieldThree.getView());
	
	var playerFieldFour = new PlayerField( - PLAYER_FIELD_HEIGHT / 2.0, yMidForPlayerField, 90, scale);
	battleGround.add(playerFieldFour.getView());
	
	// Coins
	var coinX = width * 3.0 / 5.0;
	var coinY = height / 2.0 - COIN_SIZE * 0.6;
	
	var coinOne = new Coin(coinX + COIN_SIZE * 0.4, coinY - COIN_SIZE * 0.28);
	battleGround.add(coinOne.getView());
	
	var coinTwo = new Coin(coinX + COIN_SIZE * 0.9, coinY + COIN_SIZE * 0.75);
	battleGround.add(coinTwo.getView());
	
	var coinThree = new Coin(coinX - COIN_SIZE * 0.4, coinY + COIN_SIZE * 0.6);
	battleGround.add(coinThree.getView());
	
	// Dies
	var dieX = width * 2.0 / 6.0;
	var dieY = height / 2.0 - DIE_SIZE * 0.3;
	
	var dieFour = new Die(dieX + DIE_SIZE * 0.6, dieY + DIE_SIZE * 0.2, 6, 'images/Die6.png');
	battleGround.add(dieFour.getView());
	
	var dieTwenty = new Die(dieX - DIE_SIZE * 0.5, dieY - DIE_SIZE * 0.2, 20, 'images/Die20.png');
	battleGround.add(dieTwenty.getView());
	
	// Return the battleground
	return battleGround;
}

function TwoVsTwoBattleground() {
	// Window
	var battleGround = Ti.UI.createWindow({  
	    title:'4FFA',
	    backgroundColor:'#37474A',
	    orientationModes: [ Ti.UI.LANDSCAPE_RIGHT ],
	    fullscreen: true,
	    navBarHidden: true,
	});
	
	
	// Reset Button
	resetButton = new ResetButton(0, (height - RESET_BUTTON_SIZE) / 2.0, 90);
	battleGround.add(resetButton.getView());
	
	
	// Settings
	var scale = 1.03;
	var offset = 5;
	
	// Playerfields
	var playerFieldOne = new PlayerField(offset, offset, 180, scale);
	battleGround.add(playerFieldOne.getView());
	
	var playerFieldTwo = new PlayerField(offset, height - PLAYER_FIELD_HEIGHT - offset, 0, scale);
	battleGround.add(playerFieldTwo.getView());
	
	var playerFieldThree = new PlayerField(width - PLAYER_FIELD_WIDTH - offset, offset, 180, scale);
	battleGround.add(playerFieldThree.getView());
	
	var playerFieldFour = new PlayerField(width - PLAYER_FIELD_WIDTH - offset, height - PLAYER_FIELD_HEIGHT - offset, 0, scale);
	battleGround.add(playerFieldFour.getView());
	
	// Coins
	var coinX = width * 3.0 / 5.0;
	var coinY = height / 2.0 - COIN_SIZE * 0.6;
	
	var coinOne = new Coin(coinX + COIN_SIZE * 0.4, coinY - COIN_SIZE * 0.28);
	battleGround.add(coinOne.getView());
	
	var coinTwo = new Coin(coinX + COIN_SIZE * 0.9, coinY + COIN_SIZE * 0.75);
	battleGround.add(coinTwo.getView());
	
	var coinThree = new Coin(coinX - COIN_SIZE * 0.4, coinY + COIN_SIZE * 0.6);
	battleGround.add(coinThree.getView());
	
	// Dies
	var dieX = width * 2.0 / 6.0;
	var dieY = height / 2.0 - DIE_SIZE * 0.3;
	
	var dieFour = new Die(dieX + DIE_SIZE * 0.6, dieY + DIE_SIZE * 0.2, 6, 'images/Die6.png');
	battleGround.add(dieFour.getView());
	
	var dieTwenty = new Die(dieX - DIE_SIZE * 0.5, dieY - DIE_SIZE * 0.2, 20, 'images/Die20.png');
	battleGround.add(dieTwenty.getView());
	
	// Return the battleground
	return battleGround;
}

function TwoHeadedGiantBattleground() {
	// Window
	var battleGround = Ti.UI.createWindow({  
	    title:'4FFA',
	    backgroundColor:'#37474A',
	    orientationModes: [ Ti.UI.LANDSCAPE_RIGHT ],
	    fullscreen: true,
	    navBarHidden: true,
	});
	
	
	// Reset Button
	resetButton = new ResetButton(0, (height - RESET_BUTTON_SIZE) / 2.0, 90);
	battleGround.add(resetButton.getView());
	
	
	// Settings
	var scale = 1.1;
	var offset = 10;
	
	// Playerfields
	var playerFieldOne = new TwoHeadedGaintField((width - THG_FIELD_WIDTH) / 2.0, offset, 180, scale);
	battleGround.add(playerFieldOne.getView());
	
	var playerFieldTwo = new TwoHeadedGaintField((width - THG_FIELD_WIDTH) / 2.0, height - THG_FIELD_HEIGHT - offset, 0, scale);
	battleGround.add(playerFieldTwo.getView());
	
	// Coins
	var coinX = width * 3.0 / 5.0;
	var coinY = height / 2.0 - COIN_SIZE * 0.6;
	
	var coinOne = new Coin(coinX + COIN_SIZE * 0.4, coinY - COIN_SIZE * 0.28);
	battleGround.add(coinOne.getView());
	
	var coinTwo = new Coin(coinX + COIN_SIZE * 0.9, coinY + COIN_SIZE * 0.75);
	battleGround.add(coinTwo.getView());
	
	var coinThree = new Coin(coinX - COIN_SIZE * 0.4, coinY + COIN_SIZE * 0.6);
	battleGround.add(coinThree.getView());
	
	// Dies
	var dieX = width * 2.0 / 6.0;
	var dieY = height / 2.0 - DIE_SIZE * 0.3;
	
	var dieFour = new Die(dieX + DIE_SIZE * 0.6, dieY + DIE_SIZE * 0.2, 6, 'images/Die6.png');
	battleGround.add(dieFour.getView());
	
	var dieTwenty = new Die(dieX - DIE_SIZE * 0.5, dieY - DIE_SIZE * 0.2, 20, 'images/Die20.png');
	battleGround.add(dieTwenty.getView());
	
	// Return the battleground
	return battleGround;
}

function EmperorBattleground() {
	// Window
	var battleGround = Ti.UI.createWindow({  
	    title:'4FFA',
	    backgroundColor:'#37474A',
	    orientationModes: [ Ti.UI.LANDSCAPE_RIGHT ],
	    fullscreen: true,
	    navBarHidden: true,
	});
	
	
	// Reset Button
	resetButton = new ResetButton(0, (height - RESET_BUTTON_SIZE) / 2.0, 90);
	battleGround.add(resetButton.getView());
	
	
	// Settings
	var scale = 1.0;
	var offset = -2;
	
	// Playerfields
	var playerFieldOne = new EmperorField((width - EMPEROR_FIELD_WIDTH) / 2.0, offset, 180, scale);
	battleGround.add(playerFieldOne.getView());
	
	var playerFieldTwo = new EmperorField((width - EMPEROR_FIELD_WIDTH) / 2.0, height - EMPEROR_FIELD_HEIGHT - offset, 0, scale);
	battleGround.add(playerFieldTwo.getView());
	
	// Coins
	var coinX = width * 3.1 / 5.0;
	var coinY = (height - COIN_SIZE) / 2.0;
	
	var coinOne = new Coin(coinX - COIN_SIZE * 1.2, coinY);
	battleGround.add(coinOne.getView());
	
	var coinTwo = new Coin(coinX, coinY);
	battleGround.add(coinTwo.getView());
	
	var coinThree = new Coin(coinX + COIN_SIZE * 1.2, coinY);
	battleGround.add(coinThree.getView());
	
	// Dies
	var dieX = width * 2.0 / 6.5;
	var dieY = (height - DIE_SIZE) / 2.0;
	
	var dieFour = new Die(dieX + DIE_SIZE * 0.6, dieY, 6, 'images/Die6.png');
	battleGround.add(dieFour.getView());
	
	var dieTwenty = new Die(dieX - DIE_SIZE * 0.6, dieY, 20, 'images/Die20.png');
	battleGround.add(dieTwenty.getView());
	
	// Return the battleground
	return battleGround;
}

