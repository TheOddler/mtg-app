Ti.include("Battlegrounds.js");
Ti.include('PlayerField.js');
Ti.include('Coin.js');
Ti.include('Die.js');
Ti.include('Reset.js');

// this sets the background color of the master UIView (when there are no windows/tab groups on it)
Ti.UI.setBackgroundColor('#000');


var platformWidth = Titanium.Platform.displayCaps.platformWidth;
var platformHeight = Titanium.Platform.displayCaps.platformHeight;
var width = Math.max(platformWidth, platformHeight);
var height = Math.min(platformWidth, platformHeight);

var xMidForPlayerField = (width - PLAYER_FIELD_WIDTH) / 2.0;
var yMidForPlayerField = (height - PLAYER_FIELD_HEIGHT) / 2.0;

var resetButton;

if (Ti.Platform.osname == 'ipad'){
	
	var tabs = Ti.UI.createTabGroup();
	
	// Recalc some values because of the tabs
	height = height - 50;
	yMidForPlayerField = (height - PLAYER_FIELD_HEIGHT) / 2.0;
	
	
	var iconSize
	
	// Two Player
	var twoPlayerBG = TwoPlayerBattleground();
	var twoPlayerTab = Ti.UI.createTab({
		title: "1vs1",
		icon: 'images/tabs/TwoPlayers.png',
		window: twoPlayerBG
	});
	twoPlayerBG.containingTab = twoPlayerTab;
	tabs.addTab(twoPlayerTab);
	
	// Two vs Two
	var twoVsTwoPlayerBG = TwoVsTwoBattleground();
	var twoVsTwoPlayerTab = Ti.UI.createTab({
		title: "2vs2",
		icon: 'images/tabs/TwoVsTwo.png',
		window: twoVsTwoPlayerBG
	});
	twoVsTwoPlayerBG.containingTab = twoVsTwoPlayerTab;
	tabs.addTab(twoVsTwoPlayerTab);
	
	// Four Players
	var fourPlayerBG = FourPlayerBattleground();
	var fourPlayerTab = Ti.UI.createTab({
		title: "FFA",
		icon: 'images/tabs/FourPlayers.png',
		window: fourPlayerBG
	});
	fourPlayerBG.containingTab = fourPlayerTab;
	tabs.addTab(fourPlayerTab);
	
	// Two Headed Giant
	var twoHeadedGiantBG = TwoHeadedGiantBattleground();
	var twoHeadedGiantTab = Ti.UI.createTab({
		title:'2HG',
		icon: 'images/tabs/TwoHeadedGiant.png',
		window: twoHeadedGiantBG
	});
	twoHeadedGiantBG.containingTab = twoHeadedGiantTab;
	tabs.addTab(twoHeadedGiantTab);
	
	// Emperor
	var emperorBG = EmperorBattleground();
	var emperorTab = Ti.UI.createTab({
		title:"Emperor",
		icon: 'images/tabs/Emperor.png',
		window: emperorBG
	});
	emperorBG.containingTab = emperorTab;
	tabs.addTab(emperorTab);
	
	
	
	// Open the window
	tabs.open();
}
else if (Ti.Platform.osname == 'iphone') {
	// Window
	var battleGround = SinglePlayerBattlegroundiPhone();
	
	
	// Open the window
	battleGround.open();
}
else {
	// Window
	var battleGround = SinglePlayerBattleground();
	
	
	// Open the window
	battleGround.open();
}
