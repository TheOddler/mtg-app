DIE_ROLLS = 10;
DIE_ROLL_DURATION = 75;
DIE_SIZE = 90;

function Die(x, y, sides, image, scale, rotation){
	if (scale == null){
		scale = 1.0;
	}
	
	if (rotation == null){
		rotation = Math.random() * 360.0;
	}
	
 	// Create master view
 	var transform = Ti.UI.create2DMatrix({rotate: rotation});
 	transform = transform.scale(scale,scale);
 	this.view = Ti.UI.createView({
 		left:x,
 		top:y,
 		transform:transform,
		width: DIE_SIZE,
		height: DIE_SIZE,
		backgroundImage: image,
 	});
 	
 	var label = Ti.UI.createLabel({
		color: 'white',
		font: { fontSize:60, fontWeight:'bold' },
		shadowColor: 'black',
		shadowOffset: {x:'2dip',y:'2dip'},
		opacity: .9,
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
		verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
		height: Titanium.UI.FILL,
		width: Titanium.UI.FILL,
		touchEnabled:false,
	});
	label.text = Math.floor(Math.random() * sides + 1);
	this.view.add(label);
 	
 	var thisView = this.view;
 	var singleTapEventFunction = function(e){
		thisView.removeEventListener('singletap', singleTapEventFunction);

		DoRoll(0, sides, label, function() { thisView.addEventListener('singletap', singleTapEventFunction); });
	};
	
	this.view.addEventListener('singletap', singleTapEventFunction);
};

function DoRoll(rollNumber, sides, label, callback) {
	if (rollNumber < DIE_ROLLS) {
		setTimeout(function(){
				label.text = Math.floor(Math.random() * sides + 1);
				DoRoll(rollNumber + 1, sides, label, callback);
			}, DIE_ROLL_DURATION);
	}
	else {
		callback();
	}
}

Die.prototype.getView = function(){
	return this.view;
}