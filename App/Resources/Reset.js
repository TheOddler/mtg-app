RESET_BUTTON_SIZE = 60;

function ResetButton(x, y, rotation, scale){
	if (scale == null){
		scale = 1.0;
	}
	
	if (rotation == null){
		rotation = Math.random() * 360.0;
	}
	
 	// Create master view
 	var transform = Ti.UI.create2DMatrix({rotate: rotation});
 	transform = transform.scale(scale,scale);
 	this.view = Ti.UI.createView({
 		left:x,
 		top:y,
 		transform:transform,
		width: RESET_BUTTON_SIZE,
		height: RESET_BUTTON_SIZE,
		backgroundImage: 'images/Reset.png',
 	});
}

ResetButton.prototype.getView = function(){
	return this.view;
}

ResetButton.prototype.addResetEvent = function(callback){
	this.view.addEventListener('singletap', callback);
	Ti.Gesture.addEventListener('shake', callback);
}
