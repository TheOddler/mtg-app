COIN_SIZE = 65;

COIN_FLIP_COUNT = 3;
COIN_FLIP_DURATION = 70;
COIN_FLIP_JUMP_SCALE_MULTI = 1.6;

function Coin(x, y, scale, rotation){
	if (scale == null){
		scale = 1.0;
	}
	
	if (rotation == null){
		rotation = Math.random() * 360.0;
	}
	
 	// Create master view
 	var transform = Ti.UI.create2DMatrix({rotate: rotation});
 	transform = transform.scale(scale,scale);
 	this.view = Ti.UI.createView({
 		left:x,
 		top:y,
 		transform:transform,
		width: COIN_SIZE,
		height: COIN_SIZE,
 	});
 	
 	var image = Ti.UI.createView({
 		left:0,
 		top:0,
 		backgroundImage:'images/Coin.png',
		width: COIN_SIZE,
		height: COIN_SIZE,
 	});
 	this.view.add(image);
 	
 	var label = Ti.UI.createLabel({
		color: 'white',
		font: { fontSize:40, fontWeight:'bold' },
		shadowColor: 'black',
		shadowOffset: {x:'2dip',y:'2dip'},
		opacity: .9,
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
		verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
		height: Titanium.UI.FILL,
		width: Titanium.UI.FILL,
		touchEnabled:false,
	});
	if (Math.random() > .5) {
		label.text = "H";
	}
	else {
		label.text = "T";
	}
	image.add(label);
 	
 	var thisView = this.view;
 	var singleTapEventFunction = function(e){
		thisView.removeEventListener('singletap', singleTapEventFunction);
		label.text = "";
		
		var jumpScale = scale * COIN_FLIP_JUMP_SCALE_MULTI;
		var jumpTransform = Ti.UI.create2DMatrix({rotate: rotation});
		jumpTransform = jumpTransform.scale(jumpScale, jumpScale);
		var animationJump = Titanium.UI.createAnimation({
			duration: COIN_FLIP_DURATION * COIN_FLIP_COUNT,
			autoreverse: true,
			transform: jumpTransform,
		});
		
		var animationFlip = Titanium.UI.createAnimation({
			duration: COIN_FLIP_DURATION,
			repeat: COIN_FLIP_COUNT,
			autoreverse: true,
			height: 0,
			top: COIN_SIZE / 2.0,
			right: COIN_SIZE / 2.0,
		});
		animationFlip.addEventListener('complete', function(){
			if (Math.random() > .5) {
				label.text = "H";
			}
			else {
				label.text = "T";
			}
			
			thisView.addEventListener('singletap', singleTapEventFunction);
		});
		
		thisView.animate(animationJump);
		image.animate(animationFlip);
	};
	
	this.view.addEventListener('singletap', singleTapEventFunction);
};

Coin.prototype.getView = function(){
	return this.view;
}



