/**
 * @author Pablo Bollansée
 */
var MAX_HEALTH = 200;
var INITIAL_HEALTH = 20;
var MAX_MANA = 50;

var PLAYER_FIELD_WIDTH = 503;
var PLAYER_FIELD_HEIGHT = 252;

var THG_FIELD_WIDTH = 806;
var THG_FIELD_HEIGHT = 252;

var EMPEROR_FIELD_WIDTH = 806;
var EMPEROR_FIELD_HEIGHT = 327;

function PlayerField(x, y, rotation, scale){
	if (scale == null){
		scale = 1.0;
	}
	if (rotation == null){
		rotation = 0.0;
	}
	
 	// Create master view
 	var transform = Ti.UI.create2DMatrix({rotate:rotation});
 	transform = transform.scale(scale,scale);
 	this.view = Ti.UI.createView({
 		left:x,
 		top:y,
 		transform:transform,
 		backgroundImage:'images/PlayerField.png',
		width: PLAYER_FIELD_WIDTH,
		height: PLAYER_FIELD_HEIGHT,
 	});
 	
 	// Add life
	this.view.add(createCounter('210dip', '-12dip', '180dip', '214dip', 80, 20, -1));
 	
 	// Add mana
 	this.view.add(createManaCounters('17dip', '15dip'));
 	
 	// Add poison counter
 	this.view.add(createCounter('197dip', '130dip', '66dip', '100dip', 30, 0, 1, 0));
 	
 	// Wins
	this.view.add(createCounter('391dip', '-16dip', '80dip', '116dip', 40, 0, 1, 0, false));
 	
 	// Stars
 	//this.view.add(createCounter('330dip', '155dip', '64dip', '100dip', 40, 0, 1, 0));
 	this.view.add(createCounter('375dip', '100dip', '80dip', '120dip', 40, 0, 1, 0));
};

function TwoHeadedGaintField(x, y, rotation, scale) {
	if (scale == null){
		scale = 1.0;
	}
	if (rotation == null){
		rotation = 0.0;
	}
	
 	// Create master view
 	var transform = Ti.UI.create2DMatrix({rotate:rotation});
 	transform = transform.scale(scale,scale);
 	this.view = Ti.UI.createView({
 		left:x,
 		top:y,
 		transform:transform,
 		backgroundImage:'images/TwoHeadedGiantField.png',
		width: THG_FIELD_WIDTH,
		height: THG_FIELD_HEIGHT,
 	});
 	
 	// Add life
	this.view.add(createCounter('313dip', '-12dip', '180dip', '214dip', 80, 30, -1));
 	
 	// Add mana
 	this.view.add(createManaCounters('19dip', '15dip'));
 	this.view.add(createManaCounters('616dip', '15dip'));
 	
 	// Add poison counter
 	this.view.add(createCounter('238dip', '125dip', '80dip', '110dip', 30, 0, 1, 0));
 	
 	// Wins
	this.view.add(createCounter('488dip', '125dip', '80dip', '110dip', 40, 0, 1, 0, false));
 	
 	// Stars
 	this.view.add(createCounter('208dip', '15dip', '80dip', '110dip', 40, 0, 1, 0));
 	this.view.add(createCounter('516', '15dip', '80dip', '110dip', 40, 0, 1, 0));
}

function EmperorField(x, y, rotation, scale) {
	if (scale == null){
		scale = 1.0;
	}
	if (rotation == null){
		rotation = 0.0;
	}
	
 	// Create master view
 	var transform = Ti.UI.create2DMatrix({rotate:rotation});
 	transform = transform.scale(scale,scale);
 	this.view = Ti.UI.createView({
 		left:x,
 		top:y,
 		transform:transform,
 		backgroundImage:'images/EmperorField.png',
		width: EMPEROR_FIELD_WIDTH,
		height: EMPEROR_FIELD_HEIGHT,
 	});
 	
 	// Add life
	this.view.add(createCounter('313dip', '80dip', '180dip', '214dip', 80, 20, -1));
	this.view.add(createCounter('47dip', '-12dip', '170dip', '204dip', 76, 20, -1));
	this.view.add(createCounter('585dip', '-12dip', '170dip', '204dip', 76, 20, -1));
 	
 	// Add mana
 	this.view.add(createCounter('20dip', 	'198dip',	'57dip',	'111dip', 40, 0, 1, 0));
 	this.view.add(createCounter('77dip', 	'198dip',	'57dip',	'111dip', 40, 0, 1, 0));
 	this.view.add(createCounter('134dip', 	'198dip',	'57dip',	'111dip', 40, 0, 1, 0));
 	
 	this.view.add(createCounter('612dip', 	'198dip',	'57dip',	'111dip', 40, 0, 1, 0));
 	this.view.add(createCounter('669dip', 	'198dip', 	'57dip',	'111dip', 40, 0, 1, 0));
 	this.view.add(createCounter('726dip', 	'198dip', 	'57dip',	'111dip', 40, 0, 1, 0));
 	
 	// Add poison counter
 	this.view.add(createCounter('238dip', '200dip', '80dip', '110dip', 30, 0, 1, 0));
 	
 	// Wins
	this.view.add(createCounter('488dip', '200dip', '80dip', '110dip', 40, 0, 1, 0, false));
 	
 	// Stars
 	this.view.add(createCounter('208dip', '90dip', '80dip', '110dip', 40, 0, 1, 0));
 	this.view.add(createCounter('516', '90dip', '80dip', '110dip', 40, 0, 1, 0));
}

PlayerField.prototype.getView = function(){
	return this.view;
}

TwoHeadedGaintField.prototype.getView = function(){
	return this.view;
}

EmperorField.prototype.getView = function(){
	return this.view;
}


function createManaCounters(x,y){
	var counters = Ti.UI.createView({
 		left:x,
 		top:y,
 		width:'171dip',
 		height:'222dip',
 	});
 	
 	counters.add(createCounter(0, 			0, 			'57dip', '111dip', 40, 0, 1, 0));
 	counters.add(createCounter(0, 			'111dip',	'57dip', '111dip', 40, 0, 1, 0));
 	counters.add(createCounter('57dip', 	0, 			'57dip', '111dip', 40, 0, 1, 0));
 	counters.add(createCounter('57dip', 	'111dip', 	'57dip', '111dip', 40, 0, 1, 0));
 	counters.add(createCounter('114dip', 	0, 			'57dip', '111dip', 40, 0, 1, 0));
 	counters.add(createCounter('114dip', 	'111dip', 	'57dip', '111dip', 40, 0, 1, 0));
 	
 	return counters;
}

function createCounter(x, y, width, height, fontSize, initialValue, tapValue, dontShowValue, doReset){
	if (doReset == null) {
		doReset = true;
	}
	
	var counterView = Ti.UI.createView({
 		left:x,
 		top:y,
		width:width,
 		height:height,
 		
 		//backgroundColor:'red',
 		//opacity:.5,
 	});
	
	var count = initialValue;
	var label = Ti.UI.createLabel({
		color: 'white',
		font: { fontSize:fontSize, fontWeight:'bold' },
		shadowColor: 'black',
		shadowOffset: {x:'2dip',y:'2dip'},
		opacity: .9,
		textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
		verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_CENTER,
		height: Titanium.UI.FILL,
		width: Titanium.UI.FILL,
		touchEnabled:false,
	});
	updateCounterLabel(label,count,dontShowValue);
	counterView.add(label);
	
	var touchView = Ti.UI.createView({
		width:height,
		height:width,
		transform:Ti.UI.create2DMatrix({ rotate: 90 }),
	})
	touchView.addEventListener('swipe', function(e){
		if (e.direction == 'left'){
			++count;
		}
		else if(count > 0){
			--count;
		}
		
		updateCounterLabel(label,count,dontShowValue);
	});
	
	/*touchView.addEventListener('singletap', function(e){
		count += tapValue;
		updateCounterLabel(label,count,dontShowValue);
	});*/
	
	var resetFunction = function(e){
		count = initialValue;
		updateCounterLabel(label,count,dontShowValue);
	};
	touchView.addEventListener('longpress', resetFunction);
	
	if (doReset) {
		resetButton.addResetEvent(resetFunction);
	}
	
	counterView.add(touchView);
	
	return counterView;
}

function updateCounterLabel(label, newValue, dontShowValue){
	if (newValue != dontShowValue){
		label.text = newValue;
	}
	else{
		label.text = '';
	}
}

